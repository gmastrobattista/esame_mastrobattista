<?php

namespace App\Http\Controllers;

use App\Models\Course;

use Illuminate\Http\Request;

class CoursesController extends Controller {

    public function create(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'string',
            'code' => 'required|string'
        ]);

        $data = $request->all();
        $course = new Course($data);
        $course->save();

        return $this->success($course, 201);
    }

    public function index(Request $request) {
        $course = Course::all();
        return $this->success($course);
    }

    public function show(Request $request, $id) {
        $course = Course::find($id);
        return $this->success($course);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'string',
            'description' => 'string',
            'code' => 'string'
        ]);

        $course = Course::find($id);

        if (!$course) return $this->failed('Corso non trovato');

        $data = $request->all();
        $course->update($data);
        return $this->success($course);
    }

    public function destroy(Request $request, $id) {
        $course = Course::find($id);

        if (!$course) return $this->failed('Corso non trovato');

        $course->delete();
        return $this->success('Course eliminato!');
    }
}