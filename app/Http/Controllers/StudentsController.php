<?php

namespace App\Http\Controllers;

use App\Models\Student;

use Illuminate\Http\Request;

class StudentsController extends Controller {

    public function create(Request $request) {
        $this->validate($request, [
            'serial' => 'required|string',
            'full_name' => 'required|string',
            'email' => 'required|email|unique:students,email'
        ]);

        $data = $request->all();
        $student = new Student($data);
        $student->save();

        return $this->success($student, 201);
    }

    public function index(Request $request) {
        $student = Student::all();
        return $this->success($student);
    }

    public function destroy(Request $request, $id) {
        $student = Student::find($id);

        if (!$student) return $this->failed('Studente non trovato');

        $student->delete();
        return $this->success('Studente eliminato!');
    }

    public function subscriptions(Request $request, $id) {
        $student = Student::find($id);

        if (!$student) return $this->failed('The student isnt present in db');

        $subscriptions = $student->subscriptions;
        return $this->success($subscriptions);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'serial' => 'string',
            'full_name' => 'string',
            'email' => 'string|unique:students,email'
        ]);

        $student = Student::find($id);

        if (!$student) return $this->failed('Studente non trovato');

        $data = $request->all();
        $student->update($data);
        return $this->success($student);
    }
}