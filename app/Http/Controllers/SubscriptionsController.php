<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\Student;

use Illuminate\Http\Request;

class SubscriptionsController extends Controller {

    public function index(Request $request) {

        $subscription = Subscription::all();
        return $this->success($subscription);
    }

    public function create(Request $request) {
        $this->validate($request, [
            'student_id' => 'required|numeric|exists:students,id',
            'course_id' => 'required|numeric|exists:courses,id'
        ]);

        $student_id = $request->input('student_id');
        $course_id = $request->input('course_id');

        if ($this->check_subscription_already_present($student_id, $course_id)) {
            return $this->failed('Subscription already present!');
        }

        $subscription = new Subscription();
        $subscription->student_id = $student_id;
        $subscription->course_id = $course_id;
        $subscription->save();

        return $this->success($subscription, 201);
    }

    public function destroy(Request $request, $id) {

        $subscription = Subscription::find($id);

        if (!$subscription) return $this->failed('Subscription non trovata!');

        $subscription->delete();
        return $this->success('Subscription eliminata!');
    }

    private function check_subscription_already_present($student_id, $course_id) {
        $student = Student::find($student_id);
        $subscriptions = $student->subscriptions;

        foreach ($subscriptions as $subscription) {
            if ($subscription->course_id == $course_id) {
                return true;
            }
        }
        return false;
    }

    
}