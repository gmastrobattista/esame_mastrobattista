<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {
    protected $fillable = [
        'serial', 'full_name', 'email'
    ];

    public function subscriptions() {
        return $this->hasMany('App\Models\Subscription');
    }
}