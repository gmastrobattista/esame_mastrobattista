<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model {

    public function course() {
        return $this->belongsTo('App\Models\Course');
    }

    public function student() {
        return $this->belongsTo('App\Models\Student');
    }
}