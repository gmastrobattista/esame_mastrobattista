<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {
    protected $fillable = [
        'name', 'description', 'code'
    ];

    public function subscriptions() {
        return $this->hasMany('App\Models\Subscription');
    }
}